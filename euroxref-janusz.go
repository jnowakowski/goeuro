package main

import (
	"encoding/xml"
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
	"strconv"
	"strings"
)

type XMLEnvelope struct {
	Gesmes  string   `xml:"gesmes,attr"`
	Xmlns   string   `xml:"xmlns,attr"`
	Subject string   `xml:"subject"`
	Name    string   `xml:"Sender>name"`
	Days    []XMLDay `xml:"Cube>Cube"`
}

type XMLDay struct {
	Time          string            `xml:"time,attr"`
	CurrencyRates []XMLCurrencyRate `xml:"Cube"`
}

type XMLCurrencyRate struct {
	Currency string `xml:"currency,attr"`
	Rate     string `xml:"rate,attr"`
}

var uri = "https://www.ecb.europa.eu/stats/eurofxref/eurofxref-hist-90d.xml"

func showUsage() {
	fmt.Printf("Usage: \n\t$ %s YYYY-MM-DD amount src_currency dst_currency\n", os.Args[0])
	fmt.Printf("Example: \n\t$ %s 2016-09-01 100 SEK GBP\n", os.Args[0])
}

func getParameters(args []string) (string, float64, string, string) {
	if len(args) < 5 {
		showUsage()
		os.Exit(-1)
	}
	srcAmount, err := strconv.ParseFloat(args[2], 64)
	if err != nil {
		fmt.Printf("%s\n%s is not a float\n", err, args[2])
		showUsage()
		os.Exit(-1)
	}
	// TODO: we might put here some more parameters sanitization
	// example: parse date and check if it is < 90 days
	return args[1], srcAmount, strings.ToUpper(args[3]), strings.ToUpper(args[4])
}

func getExchangeRates(day string, srcAmount float64, srcCurrency string, dstCurrency string) (float64, float64) {
	var money XMLEnvelope
	var srcToEuroRate, euroToDstRate float64 = -1, -1

	// get the XML from web
	resp, err := http.Get(uri)
	if err != nil {
		fmt.Println(err)
		os.Exit(-1)
	}
	// close after
	defer resp.Body.Close()
	// read data
	xmlData, _ := ioutil.ReadAll(resp.Body)
	if err != nil {
		fmt.Println(err)
		os.Exit(-1)
	}

	// unmarshal raw data
	xml.Unmarshal(xmlData, &money)

	// what if source or destination currency is already Euro?
	if srcCurrency == "EUR" {
		srcToEuroRate = 1
	}
	if dstCurrency == "EUR" {
		euroToDstRate = 1
	}

	// iterate to find exchange rates for given day
	for _, date := range money.Days {
		// got exchange rates, skip unnecessary iterations-
		if srcToEuroRate != -1 && euroToDstRate != -1 {
			break
		}
		if date.Time == day {
			fmt.Println(date.Time)
			// look for exchange rates for given date
			for _, exchangeRate := range date.CurrencyRates {
				// found source currency rate-
				if srcCurrency == exchangeRate.Currency {
					srcToEuroRate, err = strconv.ParseFloat(exchangeRate.Rate, 64)
					if err != nil {
						fmt.Println(err)
						os.Exit(-1)
					}
					fmt.Println("\t1", exchangeRate.Currency, "=", srcToEuroRate, "EUR")
				}
				// found destination currency rate
				if dstCurrency == exchangeRate.Currency {
					euroToDstRate, err = strconv.ParseFloat(exchangeRate.Rate, 64)
					if err != nil {
						fmt.Println(err)
						os.Exit(-1)
					}
					fmt.Println("\t1", exchangeRate.Currency, "=", euroToDstRate, dstCurrency)
				}
			}
		}
	}

	// fail if cannot find rates at given date,
	if srcToEuroRate == -1 || euroToDstRate == -1 {
		fmt.Println("Could not find exchange rate for given currencies -- or requested date out of scope.")
		// TODO: user might make a typo in currencies (GPB instead of GBP)
		// Can implement prompting user with propper currency (play with Levenshtein distance)
		os.Exit(-1)
	}
	return srcToEuroRate, euroToDstRate
}

func main() {
	// get parameterrs from command line
	day, srcAmount, srcCurrency, dstCurrency := getParameters(os.Args)
	// get exchange rates from XML
	srcToEuroRate, euroToDstRate := getExchangeRates(day, srcAmount, srcCurrency, dstCurrency)
	// calculate results, print sumary
	result := srcAmount * (euroToDstRate / srcToEuroRate)
	fmt.Printf("Source amount\t: %.4f %s\n", srcAmount, srcCurrency)
	fmt.Printf("Converted to\t: %.4f %s\n", result, dstCurrency)
}
